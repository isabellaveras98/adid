package br.com.sudv.enfermeiros.crud.data.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.github.dozermapper.core.Mapping;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@SuppressWarnings("rawtypes")
@NoArgsConstructor
@JsonPropertyOrder({"id","nome", "coren","email"})
public class EnfermeiroVO extends RepresentationModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Mapping("id")
    @JsonProperty("id")
    private Long key;
    private String nome;
    private String coren;
    private String email;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EnfermeiroVO that = (EnfermeiroVO) o;
        return key == that.key && Objects.equals(nome, that.nome) && Objects.equals(coren, that.coren) && Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, nome, coren, email);
    }
}
