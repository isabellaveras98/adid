package br.com.sudv.enfermeiros.crud.data.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "tb_ala")
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Ala {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(nullable = false, length = 80)
    public String nome;

    @Column(nullable = false, length = 10)
    public String andar;


}
