package br.com.sudv.enfermeiros.crud.controller;

import br.com.sudv.enfermeiros.crud.data.model.Ala;
import br.com.sudv.enfermeiros.crud.data.model.Enfermeiro;
import br.com.sudv.enfermeiros.crud.repository.AlaRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/alas")
public class AlaController {

    private final AlaRepository alaRepository;

    public AlaController(AlaRepository alaRepository) {
        this.alaRepository = alaRepository;
    }

    @GetMapping("/nova-ala")
    public String showSignUpForm(Ala ala) {
        return "add-ala";
    }

    @GetMapping()
    public String requestAllAlas(Model model) {
        model.addAttribute("alas", alaRepository.findAll());
        return "alas";
    }

    @PostMapping(value = "/addala")
    public String addUser(@Valid Ala ala, BindingResult result){
        if (result.hasErrors()) {
            return "add-ala";
        }
        alaRepository.save(ala);
        return "redirect:/alas";
    }

    @GetMapping("/delete/{id}")
    public String deleteAla(@PathVariable("id") Long id) {
        Ala ala = alaRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Id do ala invalido:" + id));
        alaRepository.delete(ala);
        return "redirect:/alas";
    }

    @PostMapping(value = "/update/{id}")
    public String updateAla(@PathVariable("id") Long id, @Valid Ala ala,
                             BindingResult result) {
        if (result.hasErrors()) {
            ala.setId(id);
            return "update-alas";
        }

        alaRepository.save(ala);
        return "redirect:/alas";
    }


    @GetMapping(value = "/update-ala/{id}")
    public String showUpdateForm(@PathVariable("id") Long id, Model model) {
        Ala ala = alaRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Id da ala invalido:" + id));

        model.addAttribute("enfermeiro", ala);
        return "update-ala";
    }
}
