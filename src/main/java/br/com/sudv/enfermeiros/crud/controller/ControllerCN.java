package br.com.sudv.enfermeiros.crud.controller;

import br.com.sudv.enfermeiros.crud.data.model.Ala;
import br.com.sudv.enfermeiros.crud.data.model.Enfermeiro;
import br.com.sudv.enfermeiros.crud.data.model.Escalacao;
import br.com.sudv.enfermeiros.crud.data.vo.EnfermeiroVO;
import br.com.sudv.enfermeiros.crud.repository.AlaRepository;
import br.com.sudv.enfermeiros.crud.repository.CriteriaEscalaRepository;
import br.com.sudv.enfermeiros.crud.repository.EnfermeiroRepository;
import br.com.sudv.enfermeiros.crud.repository.EscalaRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/content-negotiation")
public class ControllerCN {

    private final AlaRepository alaRepository;
    private final EnfermeiroRepository enfermeiroRepository;
    private final EscalaRepository escalaRepository;
    private final CriteriaEscalaRepository criteriaEscalaRepository;

    public ControllerCN(AlaRepository alaRepository, EnfermeiroRepository enfermeiroRepository, EscalaRepository escalaRepository, CriteriaEscalaRepository criteriaEscalaRepository) {
        this.alaRepository = alaRepository;
        this.enfermeiroRepository = enfermeiroRepository;
        this.escalaRepository = escalaRepository;
        this.criteriaEscalaRepository = criteriaEscalaRepository;
    }

    @GetMapping(value = "/alas",produces = { "application/json", "application/xml","application/x-yaml"} )
    public List<Ala> requestAllAlas() {
        return alaRepository.findAll();
    }

    @GetMapping(value = "/enfermeiros",produces = { "application/json", "application/xml", "application/x-yaml"})
    public List<Enfermeiro> requestAllEnfermeiros() {
        return enfermeiroRepository.findAll();
    }

    @GetMapping(value = "/escalacao", produces = { "application/json", "application/xml", "application/x-yaml"})
    public List<Escalacao> showUserList() {
        return criteriaEscalaRepository.findAll();
    }

    @GetMapping(value = "/enfermeiros/{coren}",produces = { "application/json", "application/xml","application/x-yaml"} )
    public EnfermeiroVO findAlaById(@PathVariable("coren") String coren) {

        Enfermeiro enfermeiro = enfermeiroRepository.findByCoren(coren);
            EnfermeiroVO  enfermeiroVO = new EnfermeiroVO(enfermeiro.getId(),enfermeiro.getNome(),enfermeiro.getCoren(),enfermeiro.getEmail());
        enfermeiroVO.add(linkTo(methodOn(ControllerCN.class).findAlaById(coren)).withSelfRel());
        return enfermeiroVO;
    }
}
