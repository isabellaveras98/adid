--
-- Índices para tabela `tb_ala`
--
ALTER TABLE `tb_ala`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `tb_enfermeiro`
--
ALTER TABLE `tb_enfermeiro`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `tb_escala`
--
ALTER TABLE `tb_escala`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKs020gbxsqd4kkqvyw8e114ht4` (`id_ala`),
  ADD KEY `FKthx95rke04k01ci69pg66t3v5` (`id_enfermeiro`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `tb_ala`
--
ALTER TABLE `tb_ala`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `tb_enfermeiro`
--
ALTER TABLE `tb_enfermeiro`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `tb_escala`
--
ALTER TABLE `tb_escala`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `tb_escala`
--
ALTER TABLE `tb_escala`
  ADD CONSTRAINT `FKs020gbxsqd4kkqvyw8e114ht4` FOREIGN KEY (`id_ala`) REFERENCES `tb_ala` (`id`),
  ADD CONSTRAINT `FKthx95rke04k01ci69pg66t3v5` FOREIGN KEY (`id_enfermeiro`) REFERENCES `tb_enfermeiro` (`id`);
COMMIT;
